# xyz-company-front-end

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Identified Issues
```
1. Navbar 'Pending Posts' nav link doesn't hide/show as soon as the relevant person logged in. Need to reload to get apply the condition.
2. It doesn't reload as soon as a commment has been added to a post. Need to reload the page and comments will be shown.
3. A post supposed to have an Image but I couldn't finish the feature.
```
