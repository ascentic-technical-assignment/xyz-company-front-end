import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import TheCard from './components/base/TheCard';
import TheButton from './components/base/TheButton';

const app = createApp(App);
app.use(router);
app.use(store);
app.component('the-card', TheCard);
app.component('the-button', TheButton);
app.mount('#app');
