import { createRouter, createWebHistory } from "vue-router";
import PostsList from './pages/posts/PostsList';
import PostDetails from './pages/posts/PostDetails';
import PendingPostsList from './pages/admin/PendingPostsList';
import Register from './pages/auth/Register';
import Login from './pages/auth/Login';
import NotFound from './pages/errors/NotFound';
import store from './store/index';
import PostCreate from './pages/posts/PostCreate';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/posts',
            component: PostsList,
            meta: { requiresAuth: true }
        },
        {
            path: '/posts/info/:slug',
            props: true,
            component: PostDetails,
            meta: { requiresAuth: true }
        },
        {
            path: '/posts/pending',
            component: PendingPostsList,
            meta: { requiresAuth: true }
        },
        {
            path: '/posts/create',
            component: PostCreate,
            meta: { requiresAuth: true }
        },
        {
            path: '/register',
            component: Register
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/notFound(.*)',
            component: NotFound
        }
    ]
});

router.beforeEach(function(to, _, next) {
    if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
        next('/login');
    } else if (to.meta.requiresUnauth && store.getters.isAuthenticated) {
        next('/posts');
    } else {
        next();
    }
});

export default router;