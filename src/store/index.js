import { createStore } from "vuex";
import postModule from './modules/posts/index';
import pendingPostModule from './modules/pending-posts/index';
import authModule from './modules/auth/index';
import commentModule from './modules/comments/index';

const store = createStore({
    modules: {
        posts: postModule,
        pendingPosts: pendingPostModule,
        auth: authModule,
        comments: commentModule
    }
});

export default store;