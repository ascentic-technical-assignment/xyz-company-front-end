export default {
    async login(context, payload) {
        const response = await fetch('http://127.0.0.1:8000/api/login', {
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            body: JSON.stringify({
                email: payload.email,
                password: payload.password
            })
        });
        const responseData = await response.json();
        if (!response.ok) {
            throw responseData.errors || responseData.message;
        } else {
            if(responseData.status) {
                localStorage.setItem('token', responseData.token);
                localStorage.setItem('userId', responseData.user_id);
                localStorage.setItem('userType', responseData.user_type);
                context.commit('setUser', {
                    token: responseData.token,
                    userId: responseData.user_id,
                    userType: responseData.user_type
                });
            } else {
                throw {failed: responseData.message};
            }
        }
    },
    async register(context, payload) {
        const response = await fetch('http://127.0.0.1:8000/api/register', {
            method: 'POST',
            headers: {
                Accept: 'application/json'
            },
            body: JSON.stringify({
                name: payload.name,
                email: payload.email,
                password: payload.password,
                confirm_password: payload.confirm_password
            })
        });
        const responseData = await response.json();
        if (!response.ok) {
            throw responseData.errors || responseData.message;
        } else {
            if(responseData.status) {
                localStorage.setItem('token', responseData.token);
                localStorage.setItem('userId', responseData.user_id);
                localStorage.setItem('userType', responseData.user_type);
                context.commit('setUser', {
                    token: responseData.token,
                    userId: responseData.user_id,
                    userType: responseData.user_type
                });
            } else {
                throw {failed: responseData.message};
            }
        }
    },
    logout(context) {
        localStorage.removeItem('token');
        localStorage.removeItem('userId');
        localStorage.removeItem('userType');
        context.commit('setUser', {
            token: null,
            userId: null,
            userType:null
        });
    },
};