import actions from './actions';
import getters from './getters';
import mutations from './mutations';

export default {
    state() {
        return {
            token: localStorage.getItem('token') !== null ? localStorage.getItem('token') : null,
            userId: localStorage.getItem('userId') !== null ? localStorage.getItem('userId') : null,
            userType: localStorage.getItem('userType') !== null ? localStorage.getItem('userType') : null
        }
    },
    mutations,
    actions,
    getters
}