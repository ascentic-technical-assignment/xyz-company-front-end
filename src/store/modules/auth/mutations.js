export default {
    setUser(state, payload) {
        state.token = payload.token;
        state.userId = payload.user_id;
    },
};