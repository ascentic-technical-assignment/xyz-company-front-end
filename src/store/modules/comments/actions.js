export default {
    async loadComments(context, payload) {
        const options = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json'
            }
        };
        const response = await fetch('http://127.0.0.1:8000/api/post/comment/list?slug=' + payload.slug, options);
        const responseData = await response.json();
        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch post comments!');
            throw error;
        } else {
            if (responseData.status) {
                return responseData.data;
            }
        }
    },
    async createComment(context, payload) {
        const response = await fetch('http://127.0.0.1:8000/api/post/comment/create', {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                Accept: 'application/json'
            },
            body: JSON.stringify({
                comment: payload.comment,
                slug: payload.slug
            })
        });
        const responseData = await response.json();
        if (!response.ok) {
            throw responseData.errors || responseData.message;
        } else {
            if(!responseData.status) {
                throw {failed: responseData.message};
            }
        }
    }
};