export default {
    comments(state) {
        return state.comments;
    },
    hasComments(_, getters) {
        return getters.comments && getters.comments.length > 0;
    }
};