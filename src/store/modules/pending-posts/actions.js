export default {
    async loadPendingPosts(context) {
        const headers = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json'
            }
        };
        const response = await fetch('http://127.0.0.1:8000/api/post/pending/list', headers);
        const responseData = await response.json();
        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch pending posts!');
            throw error;
        } else {
            if (responseData.status) {
                context.commit('setPendingPosts', responseData.data);
            } else {
                const error = new Error(responseData.message || 'Failed to fetch pending posts!');
                throw error;
            }
        }
    },
    async approvePost(context, payload) {
        const response = await fetch('http://127.0.0.1:8000/api/post/approve', {
            method: 'PUT',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                Accept: 'application/json'
            },
            body: JSON.stringify({
                slug: payload.slug
            })
        });
        const responseData = await response.json();
        if (!response.ok) {
            throw responseData.errors || responseData.message;
        } else {
            if(!responseData.status) {
                throw {failed: responseData.message};
            }
        }
    }
};