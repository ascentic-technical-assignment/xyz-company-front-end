export default {
    pendingPosts(state) {
        return state.pendingPosts;
    },
    hasPendingPosts(state) {
        return state.pendingPosts && state.pendingPosts.length > 0;
    }
};