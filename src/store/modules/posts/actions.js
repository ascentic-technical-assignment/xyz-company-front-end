export default {
    async loadPosts(context) {
        const options = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json'
            }
        };
        const response = await fetch('http://127.0.0.1:8000/api/post/list', options);
        const responseData = await response.json();
        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch posts!');
            throw error;
        } else {
            if (responseData.status) {
                context.commit('setPosts', responseData.data);
            } else {
                const error = new Error(responseData.message || 'Failed to fetch posts!');
                throw error;
            }
        }
    },
    async loadSelectedPost(context, payload) {
        const options = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                'Accept': 'application/json'
            }
        };
        const response = await fetch('http://127.0.0.1:8000/api/post/details?slug=' + payload.slug, options);
        const responseData = await response.json();
        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch the selected post!');
            throw error;
        } else {
            return responseData.data;
        }
    },
    async createPost(context, payload) {
        const response = await fetch('http://127.0.0.1:8000/api/post/create', {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                Accept: 'application/json'
            },
            body: JSON.stringify({
                title: payload.title,
                slug: payload.slug,
                body: payload.body,
                image: payload.image
            })
        });
        const responseData = await response.json();
        if (!response.ok) {
            throw responseData.errors || responseData.message;
        } else {
            if(!responseData.status) {
                throw {failed: responseData.message};
            }
        }
    },
    async deletePost(context, payload) {
        const response = await fetch('http://127.0.0.1:8000/api/post/delete', {
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token'),
                Accept: 'application/json'
            },
            body: JSON.stringify({
                slug: payload.slug
            })
        });
        const responseData = await response.json();
        if (!response.ok) {
            throw responseData.errors || responseData.message;
        } else {
            if(!responseData.status) {
                throw {failed: responseData.message};
            }
        }
    }
};