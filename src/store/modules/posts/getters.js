export default {
    posts(state) {
        return state.posts;
    },
    hasPosts(_, getters) {
        return getters.posts && getters.posts.length > 0;
    }
};